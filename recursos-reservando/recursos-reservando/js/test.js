var expect = chai.expect;

describe("Test reservarHorario(horario)", function(){
    it("Cuando se reserva un horario de un restaurant, el horario correspondiente se elimina del arreglo.", function(){
        //Declarar un objeto restaurante
        var rest = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        //llamar al metodo reservarHorario(horario) con un horario valido
        rest.reservarHorario("13:00");
        //verificar que el horario se elimine del array
        expect(rest.horarios).to.eql(["15:30", "18:00"]);
    })
    it("Cuando se reserva un horario que el restaurant no posee, el arreglo se mantiene igual.", function(){
        //Declarar un objeto restaurante
        var rest = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        //llamar al metodo reservarHorario(horario) con un horario valido
        rest.reservarHorario("19:00");
        //verificar que el horario se elimine del array
        expect(rest.horarios).to.eql(["13:00", "15:30", "18:00"]);
    })
    it("Cuando se intenta reservar un horario pero no se le pasa ningún parámetro a la función, el arreglo se mantiene igual.", function(){
        //Declarar un objeto restaurante
        var rest = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        //llamar al metodo reservarHorario(horario) con un horario valido
        rest.reservarHorario();
        //verificar que el horario se elimine del array
        expect(rest.horarios).to.eql(["13:00", "15:30", "18:00"]);
    })
})

describe('Verificar funcionalidad de obtenerPuntuación() de la clase Restaurante', function() {
    it('Dado un restaurant con determinadas calificaciones, la puntuación (que es el promedio de ellas) se calcula correctamente.', function() {
        var rest = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 8]);
        var promedio = rest.obtenerPuntuacion();
        expect(promedio).to.be.equal(7);
    });
    it('Dado un restaurant que no tiene ninguna calificación, la puntuación es igual a 0.', function() {
        var rest = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", []);
        var promedio = rest.obtenerPuntuacion();
        expect(promedio).to.be.equal(0);
    });
})















describe('Verificar funcionalidad de obtenerRestaurantes(filtroRubro, filtroCiudad, filtroHorario) de la clase Listado', function() {
    let listadoDeRestaurantes = [
        new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
        new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7]),
        new Restaurant(3, "Burgermeister", "Hamburguesa", "Berlín", ["11:30", "12:00", "22:30"], "../img/hamburguesa4.jpg", [5, 8, 4, 9, 9]),
        new Restaurant(4, "Bleecker Street Pizza", "Pizza", "Nueva York", ["12:00", "15:00", "17:30"], "../img/pizza2.jpg", [8, 9, 9, 4, 6, 7]),
        new Restaurant(5, "Jolly", "Asiática", "Berlín", ["12:00", "13:30", "16:00"], "../img/asiatica3.jpg", [8, 3, 9, 5, 6, 7])
    ];
    it('Se verifica que al buscar utilizando el filtro de Rubro, la función devuelva los restaurantes esperados.', function() {
        var listado = new Listado(listadoDeRestaurantes)
        var listadoResultado = listado.obtenerRestaurantes("Hamburguesa", null, null);
        expect(listadoResultado[0].id).to.equal(3);
    });
})