var Reserva = function(P_Horario, P_Cant_Personas, P_Precio_Persona, P_Cod_Descuento) {
    if (this.ValidarFecha(P_Horario)) {
        this.Horario = P_Horario;
    } else {
        throw new Error("El campo fecha debe ser de tipo Date");
    }
    if (P_Cant_Personas >= 1) {
        this.Cant_Personas = P_Cant_Personas;
    } else {
        throw new Error("La cantidad de personas debe ser un número mayor o igual que 1");
    }
    if (P_Precio_Persona >= 1) {
        this.Precio_Persona = P_Precio_Persona;
    } else {
        throw new Error("El precio debe ser un número mayor o igual que 1");
    }
    this.Cod_Descuento = P_Cod_Descuento;
}

Reserva.prototype.Calcular_Precio_Base = function() {
    return this.Cant_Personas * this.Precio_Persona;
}

Reserva.prototype.Calcular_Precio_Total = function() {
    let Precio_Base = this.Calcular_Precio_Base();
    let Descuento = this.Calcular_Descuento(Precio_Base);
    let Adicional = this.Calcular_Adicional(Precio_Base);
    let Total = Precio_Base + Adicional - Descuento;
    return Total;
}

Reserva.prototype.Calcular_Descuento = function(P_Precio_Base) {
    let Descuento_Grupos = this.Calcular_Descuento_Grupos(P_Precio_Base);
    let Descuento_Cupon = this.Calcular_Descuento_Cupon(P_Precio_Base);
    return Descuento_Grupos + Descuento_Cupon;
}

Reserva.prototype.Calcular_Descuento_Grupos = function(P_Precio_Base) {
    let Porcentaje_Descuento = 0;
    if (this.Cant_Personas >= 4 && this.Cant_Personas <= 6) {
        Porcentaje_Descuento = 5;
    } else if (this.Cant_Personas >= 7 && this.Cant_Personas <= 8) {
        Porcentaje_Descuento = 10;
    } else if (this.Cant_Personas > 8) {
        Porcentaje_Descuento = 15;
    }
    return this.Calcular_Monto_Con_Porcentaje(P_Precio_Base, Porcentaje_Descuento);
}

Reserva.prototype.Calcular_Descuento_Cupon = function(P_Precio_Base) {
    switch (this.Cod_Descuento) {
        case "DES15":
            return this.Calcular_Monto_Con_Porcentaje(P_Precio_Base, 15);
            break;
        case "DES200":
            return 200;
            break;
        case "DES1":
            return this.Precio_Persona;
            break;
        default:
            return 0;
            break;
    }
}

Reserva.prototype.Calcular_Adicional = function(P_Precio_Base) {
    let Adicional = 0;
    if (this.Verificar_FinDeSemana(this.Horario)) {
        Adicional += 10;
    }
    if (this.Verificar_Franja_Horaria(this.Horario, 13, 14) || this.Verificar_Franja_Horaria(this.Horario, 20, 21)) {
        Adicional += 5;
    }
    return this.Calcular_Monto_Con_Porcentaje(P_Precio_Base, Adicional);
}

Reserva.prototype.ValidarFecha = function ValidarFecha(P_Fecha) {
    if (Object.prototype.toString.call(P_Fecha) !== '[object Date]') {
        return false;
    }
    return true
}

Reserva.prototype.Verificar_FinDeSemana = function(P_Fecha) {
    let dia = (P_Fecha.getDay());
    if (dia == 5 || dia == 6 || dia == 0) {
        return true;
    } else {
        return false;
    }
}

Reserva.prototype.Verificar_Franja_Horaria = function(P_Fecha, P_Hora_Desde, P_hora_Fin) {
    var Horario_Desde = new Date(P_Fecha.getFullYear(), P_Fecha.getMonth(), P_Fecha.getDate(), P_Hora_Desde, 00);
    var Horario_Hasta = new Date(P_Fecha.getFullYear(), P_Fecha.getMonth(), P_Fecha.getDate(), P_hora_Fin, 00);
    if (P_Fecha >= Horario_Desde && P_Fecha <= Horario_Hasta) {
        return true;
    }
    return false;
}

Reserva.prototype.Calcular_Monto_Con_Porcentaje = function(P_Monto, P_Porcentaje) {
    return Math.floor(P_Monto * P_Porcentaje) / 100;
}