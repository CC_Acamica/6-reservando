var Restaurant = function(id, nombre, rubro, ubicacion, horarios, imagen, calificaciones) {
    this.id = id;
    this.nombre = nombre;
    this.rubro = rubro;
    this.ubicacion = ubicacion;
    this.horarios = horarios;
    this.imagen = imagen;
    this.calificaciones = calificaciones;
}

Restaurant.prototype.reservarHorario = function(horarioReservado) {
    /* CCABRER - GUIA 2 - Paso 1: Se refactorizó la validación */
    // for (var i = 0; i < this.horarios.length; i++) {
    //     if (this.horarios[i] === horarioReservado) {
    //         this.horarios.splice(i, 1);
    //         return;
    //     }
    // }    
    this.horarios = this.horarios.filter(horario => horario !== horarioReservado)
}

Restaurant.prototype.calificar = function(nuevaCalificacion) {
    /*CCABRER: Se modificó la validación para que permita el número 10(detectado en las pruebas). */
    if (Number.isInteger(nuevaCalificacion) && nuevaCalificacion > 0 && nuevaCalificacion <= 10) {
        this.calificaciones.push(nuevaCalificacion);
    }
}

Restaurant.prototype.obtenerPuntuacion = function() {
    if (this.calificaciones.length === 0) {
        return 0;
    } else {
        /*CCABRER - GUIA 2 - Paso 2: Se modularizó la función */
        /*         var sumatoria = 0;
                for (var i = 0; i < this.calificaciones.length; i++) {
                    sumatoria += this.calificaciones[i]
                } 

                var promedio = sumatoria / this.calificaciones.length;
                return Math.round(promedio * 10) / 10;*/
        return promedio(this.calificaciones);
    }

}

/* CCABRER - GUIA 2 - Paso 2: */
//La funcion Sumatoria recibe un array de valores números y devuelve la suma de todos ellos.
var sumatoria = function(P_Valores) {
    var suma = 0;
    P_Valores.forEach(valor => suma += valor);
    return suma;
}

/* CCABRER - GUIA 2 - Paso 2: */
//La funcion CalcularPromedio puede recibir cualquier array con números y calcular su promedio.
var promedio = function(P_Valores) {
    var suma = sumatoria(P_Valores);
    var promedio = suma / P_Valores.length;
    return Math.round(promedio * 10) / 10;
}