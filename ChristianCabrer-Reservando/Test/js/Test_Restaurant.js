var expect = chai.expect;
describe('Verificar funcionalidad de reservarHorario(horario) de la clase Restaurant', function() {
    it('Se verifica que al pasarle un horario del array, el mismo se elimine del arreglo', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        NuevoRestaurante.reservarHorario("13:00");
        expect(NuevoRestaurante.horarios).to.not.include("13:00");
    })
    it('Se verifica que al pasarle un horario del array, el arreglo conserve los valores restantes', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let Horarios = ["15:30", "18:00"];
        NuevoRestaurante.reservarHorario("13:00");
        expect(NuevoRestaurante.horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle un horario que no esta en el listado, el arreglo de horarios no se modifique', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let Horarios = NuevoRestaurante.horarios.slice();
        NuevoRestaurante.reservarHorario("19:00");
        expect(NuevoRestaurante.horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle un horario vacio, el arreglo de horarios no se modifique', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let Horarios = NuevoRestaurante.horarios.slice();
        NuevoRestaurante.reservarHorario("");
        expect(NuevoRestaurante.horarios).to.eql(Horarios);
    })
    it('Se verifica que, al invocar a la funcion sin parámetros, el arreglo de horarios no se modifique', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let Horarios = NuevoRestaurante.horarios.slice();
        NuevoRestaurante.reservarHorario();
        expect(NuevoRestaurante.horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle dos horarios concatenados, el arreglo de horarios no se modifique', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let Horarios = NuevoRestaurante.horarios.slice();
        NuevoRestaurante.reservarHorario("13:00 18:00");
        expect(NuevoRestaurante.horarios).to.eql(Horarios);
    })
})

describe('Verificar funcionalidad de obtenerPuntuacion() de la clase Restaurant', function() {
    it('Se verifica que el calculo del promedio de un restaurante con 1 calificación sea correcto', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        let Puntuacion = NuevoRestaurante.obtenerPuntuacion();
        expect(Puntuacion).to.equal(7);
    })
    it('Se verifica que el calculo del promedio de un restaurante con mas de 1 calificación sea correcto', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7, 9, 4]);
        let Puntuacion = NuevoRestaurante.obtenerPuntuacion();
        expect(Puntuacion).to.equal(6.7);
    })
    it('Se verifica que el calculo del promedio de un restaurante sin calificaciones devuelva 0', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", []);
        let Puntuacion = NuevoRestaurante.obtenerPuntuacion();
        expect(Puntuacion).to.equal(0);
    })
})

describe('Verificar funcionalidad de calificar(nuevaCalificacion) de la clase Restaurant', function() {
    it('Se verifica que al calificar con el número entero mínimo permitido: 1, se agregue correctamente la calificación', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        NuevoRestaurante.calificar(1);
        let Calificaciones = [7, 1];
        expect(NuevoRestaurante.calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número entero máximo permitido: 10, se agregue correctamente la calificación', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        NuevoRestaurante.calificar(10);
        let Calificaciones = [7, 10];
        expect(NuevoRestaurante.calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número 0, no se agregue la calificación', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        NuevoRestaurante.calificar(0);
        let Calificaciones = NuevoRestaurante.calificaciones.slice();
        expect(NuevoRestaurante.calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número 11, no se agregue la calificación', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        NuevoRestaurante.calificar(11);
        let Calificaciones = NuevoRestaurante.calificaciones.slice();
        expect(NuevoRestaurante.calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al invocar la función con el parámetro vacio, no se modifique el array de calificaciones', function() {
        let NuevoRestaurante = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [7]);
        NuevoRestaurante.calificar();
        let Calificaciones = NuevoRestaurante.calificaciones.slice();
        expect(NuevoRestaurante.calificaciones).to.eql(Calificaciones);
    })
})