let expect = chai.expect;
describe('Verificar funcionalidad de reservarUnHorario(id, horario) de la clase Listado.', function() {
    it('Se verifica que al pasarle un horario del array, el mismo se elimine del arreglo.', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        listado.reservarUnHorario(1, "13:00")
        expect(listado.restaurantes[0].horarios).to.not.include("13:00");;
    })
    it('Se verifica que al pasarle un horario del array, el mismo tenga los valores restantes correspondientes', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes);
        let Horarios = ["15:30", "18:00"];
        listado.reservarUnHorario(1, "13:00");
        expect(listado.restaurantes[0].horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle un horario que no esta en el listado, el arreglo de horarios no se modifique', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let Horarios = listado.restaurantes[0].horarios.slice();
        listado.reservarUnHorario(1, "19:00");
        expect(listado.restaurantes[0].horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle un horario vacio, el arreglo de horarios no se modifique', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let Horarios = listado.restaurantes[0].horarios.slice();
        listado.reservarUnHorario(1, "");
        expect(listado.restaurantes[0].horarios).to.eql(Horarios);
    })
    it('Se verifica que, al pasarle dos horarios concatenados, el arreglo de horarios no se modifique', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let Horarios = listado.restaurantes[0].horarios.slice();
        listado.reservarUnHorario(1, "13:00 18:00");
        expect(listado.restaurantes[0].horarios).to.eql(Horarios);
    })
    it('Se verifica que, al invocar a la funcion sin parámetros, se devuelva el mensaje de error: No se ha encontrado ningún restaurant', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let mensaje = listado.reservarUnHorario();
        expect(mensaje).to.equal("No se ha encontrado ningún restaurant");
    })
    it('Se verifica que, al invocar a la funcion con un id de restaurante inexistente, se devuelva el mensaje de error: No se ha encontrado ningún restaurant', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let mensaje = listado.reservarUnHorario(5, "13:00");
        expect(mensaje).to.equal("No se ha encontrado ningún restaurant");
    })
})

describe('Verificar funcionalidad de calificarRestaurant(id, calificacion) de la clase Listado', function() {
    it('Se verifica que al calificar con el número entero mínimo permitido: 1, se agregue correctamente la calificación', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        listado.calificarRestaurant(1, 1);
        let Calificaciones = [6, 7, 9, 10, 5, 1];
        expect(listado.restaurantes[0].calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número entero máximo permitido: 10, se agregue correctamente la calificación', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        listado.calificarRestaurant(1, 10);
        let Calificaciones = [6, 7, 9, 10, 5, 10];
        expect(listado.restaurantes[0].calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número 0, no se agregue la calificación', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        listado.calificarRestaurant(1, 0);
        let Calificaciones = listado.restaurantes[0].calificaciones.slice();
        expect(listado.restaurantes[0].calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que al calificar con el número 11, no se agregue la calificación', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        listado.calificarRestaurant(1, 11);
        let Calificaciones = listado.restaurantes[0].calificaciones.slice();
        expect(listado.restaurantes[0].calificaciones).to.eql(Calificaciones);
    })
    it('Se verifica que, al invocar a la funcion sin parámetros, se devuelva el mensaje de error: No se ha encontrado ningún restaurant', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let mensaje = listado.calificarRestaurant();
        expect(mensaje).to.equal("No se ha encontrado ningún restaurant");
    })
    it('Se verifica que, al invocar a la funcion con un id de restaurante inexistente, se devuelva el mensaje de error: No se ha encontrado ningún restaurant', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let mensaje = listado.calificarRestaurant(5, 10);
        expect(mensaje).to.equal("No se ha encontrado ningún restaurant");
    })
})

describe('Verificar funcionalidad de buscarRestaurante(id) de la clase Listado', function() {
    it('Se verifica que al buscar un Restaurante de la lista, la función devuelva el objeto Restaurant correspondiente.', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let NuevoRestaurant = new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]);
        let RestaurantBuscado = listado.buscarRestaurante(NuevoRestaurant.id);
        expect(RestaurantBuscado).to.eql(NuevoRestaurant);
    })
    it('Se verifica que al buscar un restaurante que no está en la lista la función devuelva el mensaje de error: No se ha encontrado ningún restaurant.', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let RestaurantBuscado = listado.buscarRestaurante(5);
        expect(RestaurantBuscado).to.equal("No se ha encontrado ningún restaurant");
    })
    it('Se verifica que al no pasarle el parámetro de id la función devuelva el mensaje de error: No se ha encontrado ningún restaurant.', function() {
        let listadoDeRestaurantes = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7])
        ];
        let listado = new Listado(listadoDeRestaurantes)
        let RestaurantBuscado = listado.buscarRestaurante();
        expect(RestaurantBuscado).to.equal("No se ha encontrado ningún restaurant");
    })
})

describe('Verificar funcionalidad de obtenerRestaurantes(filtroRubro, filtroCiudad, filtroHorario) de la clase Listado', function() {
    let listadoDeRestaurantes = [
        new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
        new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7]),
        new Restaurant(3, "Burgermeister", "Hamburguesa", "Berlín", ["11:30", "12:00", "22:30"], "../img/hamburguesa4.jpg", [5, 8, 4, 9, 9]),
        new Restaurant(4, "Bleecker Street Pizza", "Pizza", "Nueva York", ["12:00", "15:00", "17:30"], "../img/pizza2.jpg", [8, 9, 9, 4, 6, 7]),
        new Restaurant(5, "Jolly", "Asiática", "Berlín", ["12:00", "13:30", "16:00"], "../img/asiatica3.jpg", [8, 3, 9, 5, 6, 7]),
        new Restaurant(6, "Green salad", "Ensalada", "Berlín", ["17:00", "19:00", "20:30"], "../img/ensalada2.jpg", [8, 3, 2, 1, 8, 7]),
        new Restaurant(7, "Osteria Da Fortunata", "Pasta", "Roma", ["13:00", "15:30", "18:00"], "../img/pasta2.jpg", [7, 7, 7, 7, 3, 9]),
        new Restaurant(8, "Cafe Francoeur", "Desayuno", "París", ["14:30", "15:30", "19:00"], "../img/desayuno1.jpg", [4, 7, 9, 8, 10]),
        new Restaurant(9, "La Trottinette", "Pasta", "París", ["16:00", "18:00", "21:30"], "../img/pasta5.jpg", [8, 8, 7, 7, 7, 7]),
        new Restaurant(10, "New London Cafe", "Desayuno", "Londres", ["12:00", "13:00", "14:30"], "../img/desayuno3.jpg", [9, 4, 6, 5, 6]),
        new Restaurant(11, "Frogburguer", "Hamburguesa", "París", ["12:00", "15:00", "17:30"], "../img/hamburguesa1.jpg", [9, 8, 5, 2, 9]),
        new Restaurant(12, "Just Salad", "Ensalada", "Nueva York", ["12:00", "15:00", "17:30"], "../img/ensalada3.jpg", [8, 1, 4, 5, 5, 7]),
        new Restaurant(13, "The Counter", "Hamburguesa", "Nueva York", ["17:00", "18:00", "19:30"], "../img/hamburguesa2.jpg", [6, 9, 7, 6, 7, ]),
        new Restaurant(14, "TGood Seed Salads & Market", "Ensalada", "Nueva York", ["17:00", "19:00", "22:30"], "../img/ensalada4.jpg", [8, 8, 8, 8, 5, 7]),
        new Restaurant(15, "Carmine's", "Pasta", "Nueva York", ["14:30", "16:30", "19:00"], "../img/pasta1.jpg", [9, 8, 5, 5, 9]),
        new Restaurant(16, "Pastasciutta", "Pasta", "Roma", ["14:30", "15:30", "19:00"], "../img/pasta3.jpg", [4, 9, 10, 9, 4, 6]),
        new Restaurant(17, "Vapiano", "Pasta", "Berlín", ["12:00", "15:00", "17:30"], "../img/pasta4.jpg", [8, 4, 6, 7, 4, 7]),
        new Restaurant(18, "Pizza Union Spitalfields", "Pizza", "Londres", ["12:00", "15:00", "17:30"], "../img/pizza1.jpg", [8, 8, 8, 4, 6, 7]),
        new Restaurant(19, "Les Deux Magots", "Desayuno", "París", ["17:00", "19:00", "22:30"], "../img/desayuno4.jpg", [8, 4, 6, 6, 7]),
        new Restaurant(20, "Pappelli", "Pizza", "París", ["12:00", "15:00", "17:30"], "../img/pizza3.jpg", [8, 4, 6, 7, 7, 9, 1]),
        new Restaurant(21, "Trattoria La Cenetta", "Pizza", "Berlín", ["12:00", "15:00", "17:30"], "../img/pizza4.jpg", [8, 4, 6, 2, 5, 7]),
        new Restaurant(22, "Byron Hoxton", "Hamburguesa", "Londres", ["14:00", "16:00", "21:30"], "../img/hamburguesa3.jpg", [4, 9, 10, 10, 6]),
        new Restaurant(23, "Chez Moi", "Ensalada", "París", ["11:00", "12:00", "14:30"], "../img/ensalada1.jpg", [8, 4, 5, 5, 5, 5]),
        new Restaurant(24, "Maison Kayser", "Desayuno", "Nueva York", ["21:00", "22:30", "15:00"], "../img/desayuno2.jpg", [9, 5, 7, 6, 7]),
    ];
    it('Se verifica que al buscar utilizando el filtro de Rubro, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(3, "Burgermeister", "Hamburguesa", "Berlín", ["11:30", "12:00", "22:30"], "../img/hamburguesa4.jpg", [5, 8, 4, 9, 9]),
            new Restaurant(11, "Frogburguer", "Hamburguesa", "París", ["12:00", "15:00", "17:30"], "../img/hamburguesa1.jpg", [9, 8, 5, 2, 9]),
            new Restaurant(13, "The Counter", "Hamburguesa", "Nueva York", ["17:00", "18:00", "19:30"], "../img/hamburguesa2.jpg", [6, 9, 7, 6, 7, ]),
            new Restaurant(22, "Byron Hoxton", "Hamburguesa", "Londres", ["14:00", "16:00", "21:30"], "../img/hamburguesa3.jpg", [4, 9, 10, 10, 6])
        ];
        let listadoResultado = listado.obtenerRestaurantes("Hamburguesa", null, null);
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando el filtro de Ciudad, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7]),
            new Restaurant(10, "New London Cafe", "Desayuno", "Londres", ["12:00", "13:00", "14:30"], "../img/desayuno3.jpg", [9, 4, 6, 5, 6]),
            new Restaurant(18, "Pizza Union Spitalfields", "Pizza", "Londres", ["12:00", "15:00", "17:30"], "../img/pizza1.jpg", [8, 8, 8, 4, 6, 7]),
            new Restaurant(22, "Byron Hoxton", "Hamburguesa", "Londres", ["14:00", "16:00", "21:30"], "../img/hamburguesa3.jpg", [4, 9, 10, 10, 6])
        ];
        let listadoResultado = listado.obtenerRestaurantes(null, "Londres", null);
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando el filtro de Horario, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(1, "TAO Uptown", "Asiática", "Nueva York", ["13:00", "15:30", "18:00"], "../img/asiatica1.jpg", [6, 7, 9, 10, 5]),
            new Restaurant(7, "Osteria Da Fortunata", "Pasta", "Roma", ["13:00", "15:30", "18:00"], "../img/pasta2.jpg", [7, 7, 7, 7, 3, 9]),
            new Restaurant(10, "New London Cafe", "Desayuno", "Londres", ["12:00", "13:00", "14:30"], "../img/desayuno3.jpg", [9, 4, 6, 5, 6]),
        ];
        let listadoResultado = listado.obtenerRestaurantes(null, null, "13:00");
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando los filtros de Rubro y Ciudad, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(12, "Just Salad", "Ensalada", "Nueva York", ["12:00", "15:00", "17:30"], "../img/ensalada3.jpg", [8, 1, 4, 5, 5, 7]),
            new Restaurant(14, "TGood Seed Salads & Market", "Ensalada", "Nueva York", ["17:00", "19:00", "22:30"], "../img/ensalada4.jpg", [8, 8, 8, 8, 5, 7]),
        ];
        let listadoResultado = listado.obtenerRestaurantes("Ensalada", "Nueva York", null);
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando los filtros de Rubro y Horario, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(3, "Burgermeister", "Hamburguesa", "Berlín", ["11:30", "12:00", "22:30"], "../img/hamburguesa4.jpg", [5, 8, 4, 9, 9]),
            new Restaurant(11, "Frogburguer", "Hamburguesa", "París", ["12:00", "15:00", "17:30"], "../img/hamburguesa1.jpg", [9, 8, 5, 2, 9])
        ];
        let listadoResultado = listado.obtenerRestaurantes("Hamburguesa", null, "12:00");
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando los filtros de Ciudad y Horario, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(2, "Mandarín Kitchen", "Asiática", "Londres", ["15:00", "14:30", "12:30"], "../img/asiatica2.jpg", [7, 7, 3, 9, 7]),
            new Restaurant(10, "New London Cafe", "Desayuno", "Londres", ["12:00", "13:00", "14:30"], "../img/desayuno3.jpg", [9, 4, 6, 5, 6]),
        ];
        let listadoResultado = listado.obtenerRestaurantes(null, "Londres", "14:30");
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando los filtros de Rubro, Ciudad y Horario, la función devuelva los restaurantes esperados.', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [
            new Restaurant(11, "Frogburguer", "Hamburguesa", "París", ["12:00", "15:00", "17:30"], "../img/hamburguesa1.jpg", [9, 8, 5, 2, 9])
        ];
        let listadoResultado = listado.obtenerRestaurantes("Hamburguesa", "París", "12:00");
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando los filtros nulos, la función devuelva todos los restaurantes', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = listado.restaurantes.slice();
        let listadoResultado = listado.obtenerRestaurantes(null, null, null);
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
    it('Se verifica que al buscar utilizando filtros erroneos, la función no devuelva nada', function() {
        let listado = new Listado(listadoDeRestaurantes)
        let listadoDeRestaurantesEsperado = [];
        let listadoResultado = listado.obtenerRestaurantes("Papas Fritas", null, null);
        expect(listadoResultado).to.eql(listadoDeRestaurantesEsperado);
    })
})