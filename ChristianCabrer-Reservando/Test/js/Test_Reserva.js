let expect = chai.expect;

describe('Verificar funcionalidad de inicialización del objeto reserva.', function() {
    it('Se verifica que se devuelva error si la fecha no es de tipo date', function() {
        expect(() => new Reserva("", 4, 250, "")).to.throw("El campo fecha debe ser de tipo Date")
    })
    it('Se verifica que se devuelva error si la cantidad de personas no es un número mayor o igual que 1', function() {
        expect(() => new Reserva(new Date(), 0, 250, "")).to.throw("La cantidad de personas debe ser un número mayor o igual que 1")
    })
    it('Se verifica que se devuelva error si el precio no es un número mayor o igual que 1', function() {
        expect(() => new Reserva(new Date(), 4, 0, "")).to.throw("El precio debe ser un número mayor o igual que 1")
    })

})

describe('Verificar funcionalidad del método Calcular_Precio_Base', function() {
    it('Se verifica que se calcule correctamente el precio base con código de descuento.', function() {
        let NuevaReserva = new Reserva(new Date(), 4, 250, "DESC1");
        expect(NuevaReserva.Calcular_Precio_Base()).to.equal(1000);
    })
    it('Se verifica que se calcule correctamente el precio base sin código de descuento.', function() {
        let NuevaReserva = new Reserva(new Date(), 4, 250, "");
        expect(NuevaReserva.Calcular_Precio_Base()).to.equal(1000);
    })
})

describe('Verificar funcionalidad del método Calcular_Precio_Total', function() {
    it('Se verifica que se calcule correctamente el precio total sin descuento por grupo grande, sin cupón de descuento y sin adicional por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 22, 00),  3, 300,  "");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(900);
    })
    it('Se verifica que se calcule correctamente el precio total sin descuento por grupo grande, con cupón de descuento DES15 y sin adicional por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 22, 00),  3, 300,  "DES15");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(765);
    })
    it('Se verifica que se calcule correctamente el precio total sin descuento por grupo grande, sin cupón de descuento y con adicional de 5% por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 13, 00),  3, 300,  "");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(945);
    })
    it('Se verifica que se calcule correctamente el precio total sin descuento por grupo grande, con cupón de descuento DES200 y con adicional de 10% por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 30, 15, 00),  3, 300,  "DES200");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(790);
    })
    it('Se verifica que se calcule correctamente el precio total con descuento por grupo grande de 5%, sin cupón de descuento y sin adicional por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 22, 00),  5, 200,  "");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(950);
    })
    it('Se verifica que se calcule correctamente el precio total con descuento por grupo grande de 10%, con cupón de descuento DES1 y sin adicional por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 22, 00),  8, 200,  "DES1");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(1240);
    })
    it('Se verifica que se calcule correctamente el precio total con descuento por grupo grande de 15%, sin cupón de descuento y con adicional de 5% por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 27, 20, 30),  10, 200,  "");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(1800);
    })
    it('Se verifica que se calcule correctamente el precio total con descuento por grupo grande de 10%, con cupón de descuento DES15 y con adicional de 15% por horario.', function() {
        let NuevaReserva = new Reserva(new  Date(2019, 10, 30, 13, 00),  8, 200,  "DES15");
        expect(NuevaReserva.Calcular_Precio_Total()).to.equal(1440);
    })
})