**Sexto proyecto de la carrera Desarrollo Web Full Stack. El objetivo del proyecto está en aprender técnicas de testing, refactoring y la metodología de desarrollo de software TDD. El html, CSS, y los archivos aplicacion.js, listado.js y restaurant.js fueron brindados por el instituto y luego refactorizados para mejorar el código.**

---

## Objetivo del proyecto
Reservando es un proyecto cuyo objetivo es poner en práctica los conceptos de refactoring, testing y TDD. Te encargarás de construir pruebas que validen el funcionamiento de la aplicación y te ayuden a detectar errores en ella. Para esto, utilizarás las bibliotecas Mocha y Chai. Luego de tener esas pruebas, mejorarás el código del proyecto o como se le dice en la industria, “refactorizarás” el código del mismo.

Además, vas a aplicar TDD (Test-driven development o desarrollo guiado por pruebas) una metodología de desarrollo de software muy utilizada en la industria.

---

## Proyecto finalizado
El proyecto finalizado se encuentra en el directorio ChristianCabrer-Reservando.
Se puede ejecutar el mismo abriendo el archivo html/index.html.

---

## Descripción de como se llevo a cabo el proyecto
* Primero diseñe los test que se iban a realizar por cada clase para probar todas las funcionalidades y alternativas de la aplicación.
* Separe los test según la clase que estaba probando en el mismo. Un html y js para la clase Restaurant, un html y js para la clase Listado y un html y js para la clase Reserva.
* Realice todos los test de los distintos métodos.
* Con las pruebas detecte un pequeño error en el método Calificar de la case restaurant, ya que permitía que la máxima calificación sea 9 en lugar de 10.
* También detecte que en el método buscarRestaurante de la clase Listado, se devolvía un mensaje si el mismo no se encontraba pero en las clases que lo llamaban no se validaba, así que agregue la validación.
* Luego realice refactoring, y modularice el código de las clases. En el código deje comentado lo que reemplace y agregue un comentario para que sea mas fácil de identificar las modificaciones.
* Finalmente realice la implementación de la nueva funcionalidad de reserva utilizando TDD.

---